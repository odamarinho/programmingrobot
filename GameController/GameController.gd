extends Node

enum MOVES { UP, DOWN, LEFT, RIGHT }

export var atual_stage = 1
export var total_moves = 20
var remaining_moves

var list_of_directions = []
var holding_plug = false
var objective_done = false

onready var possible_moves = $Panel/possible_moves
onready var list_of_moves = $Panel/list_of_moves
onready var rm_label = $Panel/rm_label
onready var actor = $Grid/Actor
onready var actor_animation = $Grid/Actor/AnimationPlayer
onready var success_panel = $Panel/success_panel
onready var lose_panel = $Panel/lose_panel

func update_rm_text():
	rm_label.text = "REMINING MOVES: " + str(remaining_moves)

func _ready():
	remaining_moves = total_moves
	possible_moves.grab_focus()
	update_rm_text()
	success_panel.hide()
	lose_panel.hide()


func _on_possible_moves_item_activated(index):
	if remaining_moves <= 0:
		return
	
	var item_text = ""
	match(index):
		MOVES.UP:
			item_text = "UP"
		MOVES.DOWN:
			item_text = "DOWN"
		MOVES.LEFT:
			item_text = "LEFT"
		MOVES.RIGHT:
			item_text = "RIGHT"
	
	list_of_directions.append(index)
	list_of_moves.add_item(item_text)
	remaining_moves -= 1
	update_rm_text()

func _on_list_of_moves_item_rmb_selected(index, at_position):
	list_of_moves.remove_item(index)
	list_of_directions.remove(index)
	remaining_moves += 1
	update_rm_text()


func _on_letsgo_button_down():
	if list_of_moves.items.size() <= 0:
		return
	
	for direction in list_of_directions:
		var dir = Vector2()
		match direction:
			MOVES.UP:
				dir = Vector2(0, -1)
			MOVES.DOWN:
				dir = Vector2(0, 1)
			MOVES.LEFT:
				dir = Vector2(-1, 0)
			MOVES.RIGHT:
				dir = Vector2(1, 0)
		
		actor.move(dir)
		yield(actor_animation, "animation_finished")
	
	list_of_moves.clear()
	list_of_directions.clear()
	possible_moves.disconnect("item_activated", self, "_on_possible_moves_item_activated")
	
	if objective_done:
		success_panel.show()
	
	else:
		lose_panel.show()


func _on_clear_button_down():
	get_tree().reload_current_scene()


func _on_next_stage_button_down():
	get_tree().change_scene("res://Stages/Stage"+str(atual_stage+1)+".tscn")
